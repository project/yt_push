<?php

namespace Drupal\yt_push\Uploader;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\media\MediaInterface;
use Drupal\yt_push\Plugin\QueueWorker\Exception\InvalidSourceVideoException;
use Drupal\yt_push\Client\YouTubeClientFactory;
use Google_Service_YouTube;
use Google_Service_YouTube_VideoSnippet;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;

/**
 * Upload videos to YouTube.
 */
class YouTubeUploader {

  /**
   * The entity type manager used to load the source field definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The YouTube client factory.
   *
   * @var \Drupal\yt_push\Client\YouTubeClientFactory
   */
  protected $youTubeClientFactory;

  /**
   * YouTube uploader pipeline factory.
   *
   * @var \Drupal\yt_push\Uploader\YouTubeUploaderPipelineFactory
   */
  protected $youTubeUploaderPipelineFactory;

  /**
   * The Google SDK client used to access YouTube.
   *
   * @var \Google_Client
   */
  protected $googleClient;

  /**
   * YouTubeUploader constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager used to load the source field definition.
   * @param \Drupal\yt_push\Client\YouTubeClientFactory $youtube_client_factory
   *   The YouTube client factory.
   * @param \Drupal\yt_push\Uploader\YouTubeUploaderPipelineFactory $youtube_uploader_pipeline_factory
   *   Pipeline factory for determining the upload pipeline for a given URI.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, YouTubeClientFactory $youtube_client_factory, YouTubeUploaderPipelineFactory $youtube_uploader_pipeline_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->youTubeClientFactory = $youtube_client_factory;
    $this->youTubeUploaderPipelineFactory = $youtube_uploader_pipeline_factory;
  }

  /**
   * Upload the given media item to YouTube.
   *
   * The media item could have either a local file, or an external URL as the
   * video source.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media to upload.
   */
  public function uploadMedia(MediaInterface $media) {
    /** @var \Drupal\yt_push\Plugin\media\Source\YouTubePushField $media_source */
    $media_source = $media->getSource();
    if ($media_source->getPluginId() !== 'yt_push_field') {
      throw new InvalidSourceVideoException('The given media is not configured as a YouTube media source.');
    }

    // Upload the video.
    // @todo Add a lock here so only one upload can happen at a time.
    $request = $this->prepareInsert($media);
    $status = $this->execute($request, $media_source->getVideoSourceUri($media));

    // Reload the entity in case an editor has made any changes during a long
    // upload.
    $media = $this->entityTypeManager->getStorage('media')->load($media->id());

    $this->setYouTubeUrl($media, $status, $media_source->getConfiguration()['source_field']);

    $media->save();
  }

  /**
   * Set the YouTube URL for a media entity.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity to set the URL for.
   * @param \Google_Service_YouTube_Video $video
   *   The YouTube video that has been uploaded.
   * @param string $field_name
   *   The name of the Drupal field to save the URL in.
   *
   * @todo This method probably belongs on the entity or field or in the
   *   video_embed_field module?
   */
  protected function setYouTubeUrl(MediaInterface $media, \Google_Service_YouTube_Video $video, $field_name) {
    // @todo I can't find a method or a constant in the SDK to generate this
    // URL.
    $youtube_url = 'https://www.youtube.com/watch?v=' . $video->getId();
    $media->{$field_name}->setValue([$youtube_url]);
  }

  /**
   * Execute an upload request to YouTube.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   Request object.
   * @param \Psr\Http\Message\UriInterface $uri
   *   URI to the file we wish to upload to YouTube.
   *
   * @return bool|\Google_Service_YouTube_Video
   *   Upload status.
   */
  protected function execute(RequestInterface $request, UriInterface $uri) {
    // @todo Check to see if this and the last defer calls are required.
    $this->googleClient->setDefer(TRUE);

    // Specify the size of each chunk of data, in bytes. Set a higher value
    // reliable connection as fewer chunks lead to faster uploads. Set a lower
    // value for better recovery on less reliable connections.
    // Note that it's important that this values is a multiple of 262144 bytes
    // (0.25 Mebibyte). Here we set a value of 1048576 bytes (1 Mebibyte) as a
    // reasonable chunk size for video. This value was taken from the PHP
    // YouTube example.
    // @see https://developers.google.com/youtube/v3/code_samples/php#upload_a_video
    $chunk_size_bytes = 1 * 1024 * 1024;

    // Create a MediaFileUpload object for resumable uploads.
    $upload = $this->getGoogleHttpMediaFileUpload($request, $chunk_size_bytes);

    $pipeline = $this->youTubeUploaderPipelineFactory->getPipeline($uri);
    $status = $pipeline->upload($upload, $chunk_size_bytes);
    $this->googleClient->setDefer(FALSE);

    return $status;
  }

  /**
   * Create a MediaFileUpload object for resumable uploads.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   Request object to pass along to the \Google_Http_MediaFileUpload object.
   * @param int $chunk_size_bytes
   *   Chunk size of each chunk of a video upload in bytes.
   *
   * @return \Google_Http_MediaFileUpload
   *   Object for a resumable media file upload to YouTube.
   */
  protected function getGoogleHttpMediaFileUpload(RequestInterface $request, $chunk_size_bytes) {
    // Parameters to MediaFileUpload are:
    // client, request, mimeType, data, resumable, chunk size.
    return new \Google_Http_MediaFileUpload(
      $this->googleClient,
      $request,
      'video/*',
      NULL,
      TRUE,
      $chunk_size_bytes
    );
  }

  /**
   * Prepare the Google client and the video insert request to upload a video.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity with the video that we want to upload to YouTube.
   *
   * @return \Psr\Http\Message\RequestInterface
   *   YouTube API insert request.
   */
  protected function prepareInsert(MediaInterface $media) {
    $configuration = $media->getSource()->getConfiguration();
    $this->googleClient = $this->youTubeClientFactory->fromOptions([], $configuration);
    $this->googleClient->setDefer(TRUE);

    $yt = new Google_Service_YouTube($this->googleClient);
    $snippet = new Google_Service_YouTube_VideoSnippet();
    $snippet->setTitle($media->label());
    $snippet->setDescription($this->getDescription($media));
    $snippet->setTags($media->getSource()->getMetadata($media, 'tags'));

    $status = new \Google_Service_YouTube_VideoStatus();
    $status->setPrivacyStatus($media->getSource()->getMetadata($media, 'privacy_status_field'));
    $video = new \Google_Service_YouTube_Video();
    $video->setSnippet($snippet);
    $video->setStatus($status);

    $contentOwner = $configuration['content_owner'];
    $contentOwnerChannel = $configuration['content_owner_channel'];
    $optParams = [
      'onBehalfOfContentOwner' => $contentOwner,
      'onBehalfOfContentOwnerChannel' => $contentOwnerChannel,
    ];
    /** @var \Psr\Http\Message\RequestInterface $request */
    $request = $yt->videos->insert('snippet,status', $video, $optParams);
    $this->googleClient->setDefer(FALSE);
    return $request;
  }

  /**
   * Get the description from the given media entity.
   *
   * This will prep the description for YouTube including:
   * - Strip all HTML tags (YouTube does not allow markup).
   * - Decode HTML entities.
   * - Trim the description.
   * - Remove angle brackets (YouTube does not allow angle brackets).
   * - Truncate to 5000 characters, which is the YouTube limit.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media entity.
   *
   * @return string
   *   Video description.
   */
  protected function getDescription(MediaInterface $media) {
    // The documentation is poor on the limits and restrictions for
    // descriptions. However, empirically:
    // - Descriptions must be <= 5000 characters.
    // - Angle brackets (<, >) are not allowed.
    $description = $media->getSource()->getMetadata($media, 'description');
    // Clean up newlines for consistency when using Unicode::truncate.
    $description = preg_replace('~\R~u', PHP_EOL, $description);
    // NOTE: Avoiding wordsafe b/c of the bug in Drupal core:
    // https://www.drupal.org/project/drupal/issues/1712106
    return Unicode::truncate(str_replace(['<', '>'], '', trim(Html::decodeEntities(strip_tags($description)))), 5000, FALSE, TRUE);
  }

}
