<?php

namespace Drupal\yt_push\Uploader;

use GuzzleHttp\Client;
use Psr\Http\Message\UriInterface;

/**
 * Given a \Psr\Http\Message\UriInterface create an uploader pipeline object.
 */
class YouTubeUploaderPipelineFactory {

  /**
   * Drupal Http client. Needed for the YouTubeUploaderHttpPipeline.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Constructs a YouTubeUploaderPipelineFactory object.
   *
   * @param \GuzzleHttp\Client $http_client
   *   Drupal HTTP client.
   */
  public function __construct(Client $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * Create a new YouTubeUploaderPipeline object.
   *
   * YouTube uploader pipeline objects are responsible for moving the actual
   * bytes of the video from a source to YouTube. The source, and thus the
   * pipeline used is determined by the $uri.
   *
   * @param \Psr\Http\Message\UriInterface $uri
   *   Source URI of the video to be uploaded to YouTube.
   *
   * @return \Drupal\yt_push\Uploader\YouTubeUploaderPipelineInterface
   *   YouTube uploader pipeline to upload the given class of URI (eg. HTTP or
   *   stream) to YouTube.
   */
  public function getPipeline(UriInterface $uri) {
    if (in_array($uri->getScheme(), ['http', 'https'])) {
      $pipeline = new YouTubeUploaderHttpPipeline($uri, $this->httpClient);
    }
    else {
      $pipeline = new YouTubeUploaderStreamWrapperPipeline($uri);
    }
    return $pipeline;
  }

}
