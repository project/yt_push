<?php

namespace Drupal\yt_push\Uploader;

use Psr\Http\Message\UriInterface;

/**
 * Base YouTube uploader pipeline object.
 */
abstract class YouTubeUploaderPipelineBase implements YouTubeUploaderPipelineInterface {

  /**
   * Video URI pointing at the video file to be uploaded to YouTube.
   *
   * @var \Psr\Http\Message\UriInterface
   */
  protected $videoUri;

  /**
   * Constructs a YouTubeUploaderPipelineBase object.
   *
   * @param \Psr\Http\Message\UriInterface $video_uri
   *   Video URI pointing at the video file to be uploaded to YouTube.
   */
  public function __construct(UriInterface $video_uri) {
    $this->videoUri = $video_uri;
  }

}
