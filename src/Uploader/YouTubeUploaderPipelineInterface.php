<?php

namespace Drupal\yt_push\Uploader;

use Google_Http_MediaFileUpload;

/**
 * Defines the operations of a YouTube uploader pipeline object.
 */
interface YouTubeUploaderPipelineInterface {

  /**
   * Upload the video file for which this object was created to YouTube.
   *
   * YouTubeUploaderPipelineInterface objects should be created in the context
   * of a URI to upload to YouTube. Each YouTubeUploaderPipelineInterface
   * supports uploading a distinct class of URI to YouTube.
   *
   * @param \Google_Http_MediaFileUpload $upload
   *   Google API object encapsulating the video file upload, chunk by chunk and
   *   resumable to YouTube.
   * @param int $chunk_size_bytes
   *   The maximum size of each chunk to upload to YouTube.
   *
   * @return bool
   *   TRUE on success, otherwise FALSE.
   */
  public function upload(Google_Http_MediaFileUpload $upload, $chunk_size_bytes);

}
