<?php

namespace Drupal\yt_push\Uploader;

use Google_Http_MediaFileUpload;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;

/**
 * Http YouTube uploader pipeline.
 *
 * Upload a video file from an external HTTP(S) source to YouTube.
 */
class YouTubeUploaderHttpPipeline extends YouTubeUploaderPipelineBase {

  /**
   * Drupal HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Constructs a YouTubeUploaderHttpPipeline object.
   *
   * @param \Psr\Http\Message\UriInterface $video_uri
   *   Video URI pointing at the video file to be uploaded to YouTube.
   * @param \GuzzleHttp\Client $http_client
   *   Drupal HTTP client.
   */
  public function __construct(UriInterface $video_uri, Client $http_client) {
    parent::__construct($video_uri);
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public function upload(Google_Http_MediaFileUpload $upload, $chunk_size_bytes) {
    $head = $this->httpClient->head($this->videoUri);
    $upload->setFileSize((int) $head->getHeader('Content-Length')[0]);
    $promise = $this->httpClient->getAsync($this->videoUri, ['stream' => TRUE]);

    $status = FALSE;

    $promise->then(
        function (ResponseInterface $response) use ($upload, $chunk_size_bytes, &$status) {
          $body = $response->getBody();
          $chunk = '';
          while (!$status && !$body->eof()) {
            do {
              $chunk .= $body->read($chunk_size_bytes);
            } while (strlen($chunk) < $chunk_size_bytes && !$body->eof());
            // Note that $chunk may have accumulated more than $chunkSizeBytes,
            // since \Psr\Http\Message\StreamInterface::read can return amounts
            // less than the size you give it. YouTube expects chunks of a
            // minimum size of 262144 bytes (0.25 Mebibyte), in multiples of
            // 262144 bytes (0.25 Mebibyte). Thus we lop off the remainder and
            // save it for the next chunk upload.
            $remainder = substr($chunk, $chunk_size_bytes);
            $status = $upload->nextChunk(substr($chunk, 0, $chunk_size_bytes));
            $chunk = $remainder;
          }
        },
        function (RequestException $e) {
          // @todo Surely there's something more reasonable, such as retrying
          // a chunk?
          throw $e;
        }
      )
      ->wait();
    return $status;
  }

}
