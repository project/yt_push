<?php

namespace Drupal\yt_push\Uploader;

use Google_Http_MediaFileUpload;

/**
 * Stream wrapper YouTube uploader pipeline.
 *
 * Upload a video file from a stream wrapper source to YouTube.
 */
class YouTubeUploaderStreamWrapperPipeline extends YouTubeUploaderPipelineBase {

  /**
   * {@inheritdoc}
   */
  public function upload(Google_Http_MediaFileUpload $upload, $chunk_size_bytes) {
    $upload->setFileSize(filesize($this->videoUri));

    // Read the media file and upload it chunk by chunk.
    $status = FALSE;
    $handle = fopen($this->videoUri, 'rb');
    while (!$status && !feof($handle)) {
      $chunk = '';
      do {
        $chunk .= fread($handle, $chunk_size_bytes);
      } while (strlen($chunk) < $chunk_size_bytes && !feof($handle));
      // @todo Save the progress (state system?) so it can be viewed.
      /** @var \Google_Service_YouTube_Video $status */
      $status = $upload->nextChunk($chunk);
    }

    fclose($handle);
    return $status;
  }

}
