<?php

namespace Drupal\yt_push\Event;

use Symfony\Component\EventDispatcher\Event;
use Drupal\media\MediaInterface;

/**
 * This event is fired just before a video is published to YouTube.
 *
 * This event allows a subscriber to control whether the video in question will
 * be published to YouTube. By calling the publish() or unpublish() methods
 * an event subscriber can change the state, which will be consulted (via the
 * checkPublish() method) before uploading to YouTube.
 */
class YouTubePublishEvent extends Event {

  /**
   * Media entity being uploaded to YouTube.
   *
   * @var \Drupal\media\MediaInterface
   */
  protected $mediaEntity;

  /**
   * Current state of the event.
   *
   * Determines whether we ultimately upload the given video to YouTube.
   *
   * @var bool
   */
  protected $doPublish;

  /**
   * YouTubePublishEvent constructor.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media entity.
   * @param bool $doPublish
   *   Default value for doPublish.
   */
  public function __construct(MediaInterface $media, bool $doPublish = TRUE) {
    $this->mediaEntity = $media;
    $this->doPublish = $doPublish;
  }

  /**
   * Get the media entity being uploaded to YouTube.
   *
   * @return \Drupal\media\MediaInterface
   *   Media entity.
   */
  public function getMedia() {
    return $this->mediaEntity;
  }

  /**
   * Toggle the state of the event to publish.
   *
   * Indicate that you want to publish the given video to YouTube by calling
   * this method.
   */
  public function publish() {
    $this->doPublish = TRUE;
  }

  /**
   * Toggle the state of the event to unpublish.
   *
   * Indicate that you do not want to publish the given video to YouTube by
   * calling this method.
   */
  public function unpublish() {
    $this->doPublish = FALSE;
  }

  /**
   * Inspect the current state of the event.
   *
   * @return bool
   *   TRUE if the given video should be published to YouTube, otherwise FALSE.
   */
  public function checkPublish() {
    return $this->doPublish;
  }

}
