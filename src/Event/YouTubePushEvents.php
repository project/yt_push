<?php

namespace Drupal\yt_push\Event;

/**
 * YouTube push events.
 *
 * Defines event names which are fired during interaction with YouTube.
 */
final class YouTubePushEvents {

  /**
   * Name of the event fired just before a video is published to YouTube.
   *
   * @see \Drupal\yt_push\Event\YouTubePublishEvent
   */
  const PUBLISH = 'yt_push.media_publish';

}
