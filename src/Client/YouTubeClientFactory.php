<?php

namespace Drupal\yt_push\Client;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Http\ClientFactory;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;
use Google_Client;
use Symfony\Component\Serializer\Encoder\JsonDecode;

/**
 * Construct a new YouTube client, using Drupal's configuration.
 */
class YouTubeClientFactory {

  /**
   * The Drupal HTTP client factory.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $drupalClientFactory;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The Drupal HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * YouTubeClientFactory constructor.
   *
   * @param \Drupal\Core\Http\ClientFactory $drupal_client_factory
   *   The Drupal HTTP factory, used to get proxy and other default HTTP
   *   settings.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The Drupal entity type manager.
   */
  public function __construct(ClientFactory $drupal_client_factory, EntityTypeManager $entity_type_manager) {
    $this->drupalClientFactory = $drupal_client_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Factory constructor for a YouTube client.
   *
   * @param array $http_config
   *   Guzzle configuration to override Drupal defaults.
   * @param array $google_auth_config
   *   YouTube client authentication configuration. Associative array with the
   *   following keys:
   *   - client_id_json: (optional) JSON credentials for the Google API.
   *   - refresh_token: (optional) Refresh token for authentication.
   *   - redirect_uri: (optional) The OAuth redirect URI.
   *
   * @return \Google_Client
   *   A constructed Google client scoped to YouTube. The returned client may or
   *   may not be fully prepared for use depending on the $google_auth_config
   *   params. This factory constructor is left intentionally liberal with
   *   respect to the auth config expected to afford maximum flexibility for
   *   callee's.
   */
  public function fromOptions(array $http_config = [], array $google_auth_config = []) {
    $this->httpClient = $this->drupalClientFactory->fromOptions($http_config);
    if (empty($google_auth_config)) {
      // Attempt to load configuration from an enabled media type.
      $google_auth_config = $this->getDefaultGoogleAuthConfig();
    }

    // Create the client.
    $google_client = new Google_Client();
    $google_client->setHttpClient($this->httpClient);
    $google_client->addScope(\Google_Service_YouTube::YOUTUBE);
    // This ensures the token is refreshed automatically for the life of the
    // client.
    $google_client->setAccessType('offline');
    if (!empty($google_auth_config['client_id_json'])) {
      $decoder = new JsonDecode(TRUE);
      $google_client->setAuthConfig($decoder->decode($google_auth_config['client_id_json'], 'json'));
    }
    if (!empty($google_auth_config['refresh_token'])) {
      $google_client->fetchAccessTokenWithRefreshToken($google_auth_config['refresh_token']);
    }
    if (!empty($google_auth_config['redirect_uri'])) {
      $google_client->setRedirectUri($google_auth_config['redirect_uri']);
    }

    return $google_client;
  }

  /**
   * Factory constructor for a YouTube client given a media object.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity for which we require a YouTube client.
   * @param array $http_config
   *   Guzzle configuration to override Drupal defaults.
   *
   * @return \Google_Client
   *   A constructed google client.
   */
  public function fromMedia(MediaInterface $media, array $http_config = []) {
    return $this->fromOptions($http_config, $media->getSource()->getConfiguration());
  }

  /**
   * Factory constructor for a YouTube client given a media type object.
   *
   * @param \Drupal\media\MediaTypeInterface $media_type
   *   The media type entity for which we require a YouTube client.
   * @param array $http_config
   *   Guzzle configuration to override Drupal defaults.
   *
   * @return \Google_Client
   *   A constructed google client.
   */
  public function fromMediaType(MediaTypeInterface $media_type, array $http_config = []) {
    return $this->fromOptions($http_config, $media_type->getSource()->getConfiguration());
  }

  /**
   * Attempt to find a set of default Google API credentials.
   *
   * Looks at all media types that have YouTube upload field configured as the
   * media source and chooses the first one.
   *
   * @return array
   *   Google client configuration array.
   */
  protected function getDefaultGoogleAuthConfig() {
    $configuration = [];
    /** @var \Drupal\media\MediaTypeInterface[] $media_types */
    $media_types = $this->entityTypeManager->getStorage('media_type')
      ->loadByProperties(['source' => 'yt_push_field']);

    if (!empty($media_types)) {
      $configuration = $media_types[0]->getSource()->getConfiguration();
    }
    return $configuration;
  }

}
