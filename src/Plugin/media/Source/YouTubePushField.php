<?php

namespace Drupal\yt_push\Plugin\media\Source;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Url;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\video_embed_field\ProviderManagerInterface;
use Drupal\video_embed_media\Plugin\media\Source\VideoEmbedField;
use Drupal\yt_push\Client\YouTubeClientFactory;
use Drupal\yt_push\Plugin\QueueWorker\Exception\InvalidSourceVideoException;
use Drupal\yt_push\ThumbnailDownloadException;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Uri;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides media source plugin for video embed field.
 *
 * @MediaSource(
 *   id = "yt_push_field",
 *   label = @Translation("YouTube upload field"),
 *   description = @Translation("Enables uploading videos to YouTube with Video Embed Field."),
 *   allowed_field_types = {"video_embed_field"},
 *   default_thumbnail_filename = "video.png"
 * )
 */
class YouTubePushField extends VideoEmbedField {

  use DependencySerializationTrait;

  /**
   * The folder where thumbnails are stored.
   *
   * @var string
   */
  protected $thumbsFolder = 'video_thumbnails';

  /**
   * The URL generator used to generate the redirect URL.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * The service used to get the current URL of the form.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * The Google Client factory.
   *
   * @var \Drupal\yt_push\Client\YouTubeClientFactory
   */
  protected $youTubeClientFactory;

  /**
   * Http client used for fetching thumbnails.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Queue to handle thumbnail downloads.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $thumbnailDownloadQueue;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   Config field type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   * @param \Drupal\video_embed_field\ProviderManagerInterface $provider_manager
   *   The video provider manager.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The URL generator used to generate the OAuth redirect URL.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The service used to get the current URL of the form.
   * @param \Drupal\yt_push\Client\YouTubeClientFactory $youtube_client_factory
   *   The YouTube client factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   HTTP client.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, FieldTypePluginManagerInterface $field_type_manager, ConfigFactoryInterface $config_factory, ProviderManagerInterface $provider_manager, UrlGeneratorInterface $url_generator, RedirectDestinationInterface $redirect_destination, YouTubeClientFactory $youtube_client_factory, ClientInterface $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory, $provider_manager);
    $this->urlGenerator = $url_generator;
    $this->redirectDestination = $redirect_destination;
    $this->youTubeClientFactory = $youtube_client_factory;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('config.factory'),
      $container->get('video_embed_field.provider_manager'),
      $container->get('url_generator'),
      $container->get('redirect.destination'),
      $container->get('yt_push.youtube_client_factory'),
      $container->get('http_client')
    );
    $instance->setThumbnailDownloadQueue($container->get('queue')->get('yt_push_thumbnail'));
    return $instance;
  }

  /**
   * Set the thumbnail download queue.
   *
   * @param \Drupal\Core\Queue\QueueInterface $queue
   *   Queue to use for thumbnail downloads.
   *
   * @return static
   *   Fluid API return.
   */
  public function setThumbnailDownloadQueue(QueueInterface $queue) {
    $this->thumbnailDownloadQueue = $queue;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return array_merge(parent::getMetadataAttributes(), [
      'privacy_status_field' => $this->t('Field which stores the privacy status of the video on YouTube. Add a checkbox field to this media type to allow configuring the privacy status.'),
      'description' => $this->t('Field which stores the description of the video on YouTube. Add a text field fo this media type to allow configuring the description.'),
      'tags' => $this->t('Field which stores the tags of the video on YouTube. Add a taxonomy field fo this media type to allow configuring the tags.'),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'client_id_json' => '',
      'content_owner' => '',
      'content_owner_channel' => '',
      'refresh_token' => '',
      'source_field' => '',
      'video_source_field' => '',
      'taxonomy' => [
        'vocabulary' => NULL,
        'terms' => NULL,
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * Get the video source URI value for the given media.
   *
   * Represents either a local video file URI or an external video URI.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media entity.
   *
   * @return \GuzzleHttp\Psr7\Uri
   *   The source video URI for the given media entity.
   */
  public function getVideoSourceUri(MediaInterface $media) {
    if (!($field_name = $this->getConfiguration()['video_source_field'])) {
      throw new InvalidSourceVideoException('The given media does not have a configured video source field. Edit the settings for your YouTube media type and select a Video source field.');
    }
    $field_definitions = $this->entityFieldManager->getFieldDefinitions('media', $media->bundle());
    if (!in_array($field_name, array_keys($field_definitions))) {
      throw new InvalidSourceVideoException('The configured video source field does not exist on the media type.');
    }
    if ($media->get($field_name)->isEmpty()) {
      throw new InvalidSourceVideoException('The video source field is empty for the given media.');
    }
    $field = $field_definitions[$field_name];
    switch ($field->getType()) {
      case 'file':
        /** @var \Drupal\file\FileInterface $file */
        if ($file = $this->entityTypeManager->getStorage('file')->load($media->{$field_name}->target_id)) {
          $uri = new Uri($file->get('uri')->getString());
        }
        else {
          throw new InvalidSourceVideoException('There is no attached video file to upload.');
        }
        break;

      case 'string':
        if (($uri = $media->{$field_name}->value) && ($uri = filter_var($uri, FILTER_VALIDATE_URL, FILTER_FLAG_SCHEME_REQUIRED | FILTER_FLAG_HOST_REQUIRED))) {
          $uri = new Uri($uri);
        }
        else {
          throw new InvalidSourceVideoException('The video source field was interpreted as an external URL and found to either be empty or invalid.');
        }
        break;

      default:
        throw new InvalidSourceVideoException(sprintf('The configured video source field type is not allowed. Expected field type is one of \'file\' or \'string\'. Actual is %s.', $field->getType()));
    }
    return $uri;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    /** @var \Drupal\media\MediaTypeInterface $media_type */
    $media_type = $this->entityTypeManager
      ->getStorage('media_type')
      ->load($media->bundle());
    $field_map = $media_type->getFieldMap();

    switch ($attribute_name) {
      case 'privacy_status_field':
        // Default to public, since we have no control over the field that is
        // used, we want to make sure we end up with something valid in case the
        // field allows empty values.
        $status = 'public';
        if (isset($field_map['privacy_status_field']) &&
          ($field_name = $field_map['privacy_status_field']) &&
          isset($media->{$field_name}) &&
          !$media->{$field_name}->isEmpty()
        ) {
          $status = $media->{$field_name}->value;
          // Allow for checkbox fields, where the value is assumed to be private
          // if the checkbox is toggled on, otherwise public as a default, or
          // off state. Also allow for option elements for choosing between the
          // three allowed privacy statuses.
          if (!in_array($status, ['unlisted', 'private', 'public'])) {
            $status = $status ? 'private' : 'public';
          }
        }
        return $status;

      case 'description':
        /** @var \Drupal\media\MediaTypeInterface $media_type */
        $media_type = $this->entityTypeManager
          ->getStorage('media_type')
          ->load($media->bundle());
        $field_name = $media_type->getFieldMap()['description'];
        $description = '';
        if (isset($media->{$field_name}) && !$media->{$field_name}->isEmpty()) {
          $description = $media->{$field_name}->value;
        }
        return $description;

      case 'tags':
        $tags = [];
        // Bunch of checks to make sure we're safe to go ahead:
        // - Ensure a field has been mapped for tags.
        // - Ensure that field exists on the given media entity.
        // - Ensure the field is non-empty.
        // - Ensure that the field is a entity reference.
        if (isset($field_map['tags']) &&
          ($field_name = $field_map['tags']) &&
          isset($media->{$field_name}) &&
          !$media->{$field_name}->isEmpty() &&
          $media->{$field_name} instanceof EntityReferenceFieldItemListInterface
        ) {
          foreach ($media->{$field_name}->referencedEntities() as $entity) {
            $tags[] = $entity->label();
          }
        }
        return $tags;

      case 'thumbnail_uri':
        try {
          return $this->getLocalThumbnailUri($media);
        }
        catch (ThumbnailDownloadException $e) {
          // If the thumbnail isn't available yet, explicitly return the default
          // thumbnail, rather than let video embed field attempt to return a
          // thumbnail by URL convention.
          $default_thumbnail_filename = $this->pluginDefinition['default_thumbnail_filename'];
          return $this->configFactory->get('media.settings')->get('icon_base_uri') . '/' . $default_thumbnail_filename;
        }
    }

    return parent::getMetadata($media, $attribute_name);
  }

  /**
   * Download the thumbnail for the given media entity.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media entity.
   *
   * @return string
   *   Locally saved thumbnail URI.
   *
   * @throws \Drupal\yt_push\ThumbnailDownloadException
   *   Thrown when there was an issue downloading the thumbnail from YouTube.
   */
  protected function downloadThumbnail(MediaInterface $media) {
    if (!$id = $this->getMetadata($media, 'id')) {
      throw new ThumbnailDownloadException(sprintf('Unable to download thumbnail from YouTube for media with id %d. Video id is not yet available.', $media->id()));
    }

    $local_uri = $this->thumbnailUri($media);
    $client = $this->youTubeClientFactory->fromMedia($media);
    $yt = new \Google_Service_YouTube($client);
    $response = $yt->videos->listVideos('snippet', ['id' => $id]);
    /** @var \Google_Service_YouTube_Video $video */
    $video = $response->getItems()[0];
    // We attempt to get the high resolution image since it's available
    // for all videos.
    if ($video && ($thumbnail = $video->getSnippet()->getThumbnails()->getHigh())) {
      $directory = $this->thumbnailDirectoryUri();
      file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
      try {
        $thumbnail = $this->httpClient->request('GET', $thumbnail->getUrl());
      }
      catch (RequestException $e) {
        throw new ThumbnailDownloadException(sprintf('Unable to download thumbnail from YouTube for media with id %d. The following error occurred during the request: %s.', $media->id(), $e->getMessage()));
      }
      file_unmanaged_save_data((string) $thumbnail->getBody(), $local_uri);
    }
    else {
      throw new ThumbnailDownloadException(sprintf('Unable to download thumbnail from YouTube for media with id %d. Thumbnail has not yet been processed on YouTube.', $media->id()));
    }

    return $local_uri;
  }

  /**
   * Get the local thumbnail URI for the given media entity.
   *
   * If the local thumbnail is already present, return it. If not, download the
   * thumbnail. In the case that the thumbnail download fails, queue it for
   * retry.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media entity.
   *
   * @return string|bool
   *   The local thumbnail URI.
   *
   * @throws \Drupal\yt_push\ThumbnailDownloadException
   *   Thrown when there was an issue downloading the thumbnail from YouTube.
   */
  protected function getLocalThumbnailUri(MediaInterface $media) {
    $local_uri = $this->thumbnailUri($media);
    if (file_exists($local_uri)) {
      return $local_uri;
    }
    else {
      try {
        return $this->downloadThumbnail($media);
      }
      catch (ThumbnailDownloadException $e) {
        // Queue for retry.
        if ($id = $media->id()) {
          $this->thumbnailDownloadQueue->createItem(['entity_id' => $media->id()]);
        }
        throw $e;
      }
    }
  }

  /**
   * Get the local thumbnail URI for a given media entity.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media entity.
   *
   * @return string
   *   The local thumbnail URI for the given media entity.
   */
  public function thumbnailUri(MediaInterface $media) {
    return $this->thumbnailDirectoryUri() . '/' . $this->getMetadata($media, 'id') . '.jpg';
  }

  /**
   * Get the thumbnail directory URI.
   *
   * @return string
   *   The thumbnail directory URI.
   */
  protected function thumbnailDirectoryUri() {
    return file_default_scheme() . '://' . $this->thumbsFolder;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $entity = $form_state->getFormObject()->getEntity();

    $redirect_url = $this->urlGenerator->generateFromRoute('yt_push.oauth_token', [], ['absolute' => TRUE]);
    $form['redirect_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Redirect URL to use in YouTube's OAuth configuration"),
      '#value' => $redirect_url,
      '#attributes' => ['readonly' => 'readonly'],
      '#description' => $this->t('The URL to redirect to after the initial authorization. This URL should be in the downloaded Client ID JSON credentials. YouTube only supports redirect URLs that are either %localhost or a public TLD. For example, %local mDNS domains are not supported. This URL is only used for the initial OAuth authorization, and the resulting token can be used with different environments. <strong>If this URL is not on your local machine it should be over HTTPS</strong>.', ['%localhost' => 'localhost', '%local' => '.local']),
    ];

    // Allow a choice for the video source field out of any available file
    // or text fields.
    $options = [MediaSourceInterface::METADATA_FIELD_EMPTY => $this->t('- Skip field -')];
    foreach ($this->entityFieldManager->getFieldDefinitions('media', $entity->id()) as $field_name => $field) {
      if ($field_name === 'name' || !($field instanceof BaseFieldDefinition) && in_array($field->getType(), ['file', 'string'])) {
        $options[$field_name] = $field->getLabel();
      }
    }

    $form['video_source_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Video source field'),
      '#description' => 'Field which stores the local video file or external URL that is uploaded to YouTube. Allowed values include a file or text field',
      '#default_value' => $this->configuration['video_source_field'],
      '#options' => $options,
    ];

    $form['client_id_json'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Client ID'),
      '#description' => $this->t('Paste the JSON credentials provided to you with a %web key.', ['%web' => 'web']),
      '#required' => TRUE,
      '#default_value' => $this->configuration['client_id_json'],
    ];

    // @todo DRACO-3734 See if we can use the API to find unique IDs from names.
    $form['content_owner'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Content owner ID'),
      '#description' => $this->t('The unique content owner string. This can be found from your content manager dashboard, such as %url.', ['%url' => 'https://www.youtube.com/dashboard?o=CONTENT_OWNER_ID']),
      '#required' => TRUE,
      '#default_value' => $this->configuration['content_owner'],
    ];

    // @todo DRACO-3575 See if we can use the API to find unique IDs from names.
    $form['content_owner_channel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Content owner channel ID'),
      '#description' => $this->t('The unique channel ID. This can be found from a channel URL, such as %url.', ['%url' => 'https://www.youtube.com/channel/CHANNEL_ID']),
      '#required' => TRUE,
      '#default_value' => $this->configuration['content_owner_channel'],
    ];

    $form['refresh_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Refresh token'),
      '#description' => $this->t('This field will be saved after completing the OAuth workflow. The refresh token can be manually set if a token has been generated previously. It is recommended to store this token in <code>settings.php</code> in an environment variable after it has been generated.'),
      '#default_value' => $this->configuration['refresh_token'],
      '#attributes' => ['autocomplete' => 'off'],
    ];

    // Configuration for taxonomy whitelisting.
    $form['taxonomy'] = [
      '#type' => 'details',
      '#title' => 'Add Taxonomy Constraint',
      '#open' => isset($this->configuration['taxonomy']['vocabulary']),
    ];

    $form['taxonomy']['vocabulary'] = [
      '#type' => 'entity_autocomplete',
      '#title' => 'Whitelist Vocabulary',
      '#description' => $this->t('Only videos with terms that are in this
        vocabulary will upload to YouTube. Leave blank to allow all videos to
        upload.'),
      '#default_value' => $this->configuration['taxonomy']['vocabulary'] ? $this->entityTypeManager->getStorage('taxonomy_vocabulary')->load($this->configuration['taxonomy']['vocabulary']) : '',
      '#target_type' => 'taxonomy_vocabulary',
    ];

    $form['taxonomy']['terms'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Whitelist Taxonomy Terms from the whitelisted
        vocabulary'),
      '#description' => $this->t('Only videos added with these terms will
        upload to YouTube. Leave blank to allow all videos with tags in the
        whitelisted vocabulary to upload.'),
      '#default_value' => isset($this->configuration['taxonomy']['terms']) ? $this->entityTypeManager->getStorage('taxonomy_term')
        ->loadMultiple(array_map(function ($term) {
                                      return $term['target_id'];
        },
                            $tids = $this->configuration['taxonomy']['terms'])) : [],
      '#target_type' => 'taxonomy_term',
      '#tags' => TRUE,
    ];

    if (!empty($this->configuration['client_id_json'])) {
      // While we can use the 'id' form field when submitting, that form field
      // is not available while building the form. So, we're stuck with two
      // different methods to get an appropriate URL to redirect to.
      $destination = Url::fromUserInput($this->redirectDestination->get())->toString();
      $form['refresh_token']['#description'] .= ' ' . $this->t('<a href="@auth-url">Generate a new refresh token</a>.', [
        '@auth-url' => $this->createAuthUrl($this->configuration['client_id_json'], $redirect_url, $destination, $entity->id()),
      ]);
    }

    // @todo DRACO-3598 Make this extendable so we can add in Content ID
    // settings which are an optional / private API.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    // Only validate the form if our fields are set. They may not be built yet
    // during the #ajax transition between media sources.
    if (isset($form['client_id_json'])) {
      try {
        $this->createAuthUrl($form_state->getValue('client_id_json'), $form_state->getValue('redirect_url'));
      }
      catch (\Exception $e) {
        $form_state->setError($form['client_id_json'], $this->t('The Client ID JSON is invalid: %message', ['%message' => $e->getMessage()]));
      }
    }

    // Check configuration for taxonomy whitelisting.
    $values = $form_state->getValues();
    if (isset($values['taxonomy']['terms'])) {
      if (!isset($values['taxonomy']['vocabulary'])) {
        $form_state->setError($form['taxonomy'], $this->t('To whitelist terms,
          you must first whitelist a vocabulary.'));
      }
      else {
        $tags = $values['taxonomy']['terms'];
        $vocabulary = $values['taxonomy']['vocabulary'];
        foreach ($tags as $tag) {
          $tagName = $this->entityTypeManager->getStorage('taxonomy_term')
            ->load($tag['target_id'])->getName();
          if (count(taxonomy_term_load_multiple_by_name($tagName, $vocabulary)) == 0) {
            $form_state->setError($form['taxonomy'], $this->t('Some of the
            whitelisted terms do not belong to the whitelisted vocabulary.'));
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // If the authorization changed, start the OAuth flow.
    if ($this->configuration['client_id_json'] != $form_state->getValue('client_id_json')) {
      // We can't use the redirect destination service here as that returns
      // the add form for the first save.
      $entire_form = $form_state->getCompleteForm();
      $machine_name = $entire_form['id']['#value'];

      // At this point, the media type has not been saved yet, so we can't load
      // it to call toUrl().
      $url = Url::fromUri($this->createAuthUrl($form_state->getValue('client_id_json'), $form_state->getValue('redirect_url'), "admin/structure/media/manage/$machine_name", $machine_name));
      $redirectResponse = new TrustedRedirectResponse($url->toString());
      $form_state->setResponse($redirectResponse);
    }

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Create an authorization URL embedding a Drupal destination.
   *
   * @param string $client_id_json
   *   The Client ID JSON.
   * @param string $redirect_uri
   *   The OAuth redirect URI.
   * @param string|null $destination
   *   (optional) A Drupal destination to redirect to after a successful OAuth
   *   transaction.
   * @param string $media_type_id
   *   (optional) Media type id being edited.
   *
   * @return string
   *   The Google OAuth URL to begin authorization with.
   */
  private function createAuthUrl($client_id_json, $redirect_uri, $destination = NULL, $media_type_id = NULL) {
    $client = $this->youTubeClientFactory->fromOptions([], ['client_id_json' => $client_id_json, 'redirect_uri' => $redirect_uri]);

    // We have to force setting the approval prompt, otherwise no refresh token
    // will be sent if one has been sent (and lost) previously.
    // https://github.com/google/google-api-php-client/issues/536#issuecomment-102717018
    $client->setApprovalPrompt('force');

    if ($destination || $media_type_id) {
      $state = [];
      if ($destination) {
        $state['destination'] = $destination;
      }
      if ($media_type_id) {
        $state['media_type_id'] = $media_type_id;
      }
      $client->setState(json_encode($state));
    }

    return $client->createAuthUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    $storage = $this->getSourceFieldStorage() ?: $this->createSourceFieldStorage();

    // Set required to FALSE as we may defer setting the video URL.
    return $this->entityTypeManager
      ->getStorage('field_config')
      ->create([
        'field_storage' => $storage,
        'bundle' => $type->id(),
        'label' => $this->pluginDefinition['label'],
        'required' => FALSE,
      ]);
  }

}
