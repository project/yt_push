<?php

namespace Drupal\yt_push\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\media\MediaInterface;
use Drupal\yt_push\Event\YouTubePushEvents;
use Drupal\yt_push\Plugin\QueueWorker\Exception\InvalidSourceVideoException;
use Drupal\yt_push\Plugin\QueueWorker\Exception\VideoAlreadyUploadedException;
use Drupal\yt_push\Uploader\YouTubeUploader;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\yt_push\Event\YouTubePublishEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Queue worker to upload videos to YouTube.
 *
 * @QueueWorker(
 *   id = "yt_push_upload",
 *   title = @Translation("Uploads videos to YouTube"),
 *   cron = {"time" = 60}
 * )
 */
class YouTubeUploadQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The factory used to retrieve the queue to add videos to.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The entity type manager used to load media entities.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The YouTube uploader.
   *
   * @var \Drupal\yt_push\Uploader\YouTubeUploader
   */
  protected $youTubeUploader;

  /**
   * Event dispatcher.
   *
   * @var \symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * YouTubeUploadQueue constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The factory used to retrieve the queue to add videos to.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager used to load media entities.
   * @param \Drupal\yt_push\Uploader\YouTubeUploader $youtube_uploader
   *   YouTube uploader.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event Dispatcher.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, QueueFactory $queue_factory, EntityTypeManagerInterface $entity_type_manager, YouTubeUploader $youtube_uploader, EventDispatcherInterface $eventDispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->queueFactory = $queue_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->youTubeUploader = $youtube_uploader;
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('queue'),
      $container->get('entity_type.manager'),
      $container->get('yt_push.youtube_uploader'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Add an item to be uploaded to YouTube.
   *
   * @param \Drupal\media\MediaInterface $video
   *   The video to be uploaded.
   *
   * @throws \Drupal\yt_push\Plugin\QueueWorker\Exception\InvalidSourceVideoException
   *   Thrown when there is no populated video field.
   * @throws \Drupal\yt_push\Plugin\QueueWorker\Exception\VideoAlreadyUploadedException
   *   Thrown when the video already has been uploaded to YouTube.
   */
  public function createItem(MediaInterface $video) {
    /** @var \Drupal\yt_push\Plugin\media\Source\YouTubePushField $mediaSource */
    $mediaSource = $video->getSource();

    if (!$mediaSource->getVideoSourceUri($video)) {
      throw new InvalidSourceVideoException('There is no attached video to upload.');
    }

    // Don't queue for upload if it's already been uploaded.
    $source_field_name = $video->getSource()->getConfiguration()['source_field'];
    if (!$video->get($source_field_name)->isEmpty()) {
      throw new VideoAlreadyUploadedException('The video has an existing YouTube URL');
    }

    $item = [
      'entity_id' => $video->id(),
    ];
    $this->queueFactory->get('yt_push_upload')->createItem($item);
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    /** @var \Drupal\media\MediaInterface $video */
    $video = $this->entityTypeManager->getStorage('media')
      ->load($data['entity_id']);

    // The video has been deleted.
    if (!$video) {
      return;
    }

    // Don't upload the video if it's already been uploaded.
    $source_field_name = $video->getSource()->getConfiguration()['source_field'];
    if (!$video->get($source_field_name)->isEmpty()) {
      return;
    }

    $event = new YouTubePublishEvent($video);
    $this->eventDispatcher->dispatch(YouTubePushEvents::PUBLISH, $event);
    if ($event->checkPublish()) {
      $this->youTubeUploader->uploadMedia($video);
    }
  }

}
