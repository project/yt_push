<?php

namespace Drupal\yt_push\Plugin\QueueWorker\Exception;

/**
 * Exception thrown when a video has already been uploaded to YouTube.
 */
class VideoAlreadyUploadedException extends \DomainException {

}
