<?php

namespace Drupal\yt_push\Plugin\QueueWorker\Exception;

/**
 * Thrown when the source video to upload can not be accessed.
 */
class InvalidSourceVideoException extends \DomainException {

}
