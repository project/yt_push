<?php

namespace Drupal\yt_push\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\RequeueException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Process a queue of media items to fetch their thumbnails from YouTube.
 *
 * @QueueWorker(
 *   id = "yt_push_thumbnail",
 *   title = @Translation("YouTube thumbnail downloader"),
 *   cron = {"time" = 60}
 * )
 */
class ThumbnailDownloader extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('queue')->get('yt_push_thumbnail')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    /** @var \Drupal\media\Entity\Media $media */
    if ($media = $this->entityTypeManager->getStorage('media')->load($data['entity_id'])) {
      // Don't attempt to update the thumbnail if the video hasn't been uploaded
      // to YouTube yet.
      /** @var \Drupal\yt_push\Plugin\media\Source\YouTubePushField $source */
      $source = $media->getSource();
      $source_field_name = $source->getConfiguration()['source_field'];
      if (!$media->get($source_field_name)->isEmpty()) {
        $media->updateQueuedThumbnail();
        $media->save();
      }

      // If we didn't get the YouTube thumbnail, re-queue for next time.
      if (!file_exists($source->thumbnailUri($media))) {
        throw new RequeueException('YouTube thumbnail is not available yet.');
      }
    }
  }

}
