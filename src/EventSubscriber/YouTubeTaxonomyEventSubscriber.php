<?php

namespace Drupal\yt_push\EventSubscriber;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\yt_push\Event\YouTubePushEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\yt_push\Event\YouTubePublishEvent;

/**
 * Taxonomy-based event subscriber for the YouTube publish event.
 *
 * Determines publish state for the video going off to YouTube by the media
 * tags.
 */
class YouTubeTaxonomyEventSubscriber implements EventSubscriberInterface, ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   * The entity type manager.
   */
  protected $entityTypeManager;

  /**
   * YouTubeTaxonomyEventSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Service creator.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[YouTubePushEvents::PUBLISH][] = ['doPublishMedia'];

    return $events;
  }

  /**
   * Event handler.
   *
   * This event handler will check the taxonomy terms of the media
   * entity, and mark the video for publish to YouTube based on that. If the
   * media has tags that correspond to the whitelisted vocabulary and,
   * whitelisted terms, then we publish, otherwise, we do not.
   *
   * @param \Drupal\yt_push\Event\YouTubePublishEvent $event
   *   YouTube publish event object.
   */
  public function doPublishMedia(YouTubePublishEvent $event) {

    // Ensure doPublish has not already been set to false.
    $video = $event->getMedia();

    $allowedTagIds = $video->getSource()
      ->getConfiguration()['taxonomy']['terms'];

    // If there are no explicitly whitelisted tags, check if there is a
    // whitelisted vocabulary instead.
    if (count($allowedTagIds) == 0) {
      $vocabulary = $video->getSource()
        ->getConfiguration()['taxonomy']['vocabulary'];

      if ($vocabulary) {
        // Else check if any tags belong to the vocabulary.
        $tags = $video->getSource()->getMetadata($video, 'tags');
        $published = FALSE;
        foreach ($tags as $tag) {
          if (
            count(
              $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties(['name' => $tag, 'vid' => $vocabulary])
            ) > 0
          ) {
            $published = TRUE;
            break;
          }
        }
        if (!$published) {
          $event->unpublish();
        }
      }
    }
    else {
      // Else check if any tags are whitelisted.
      $allowedTags = [];
      foreach ($allowedTagIds as $allowedTagId) {
        $allowedTags[] = $this->entityTypeManager->getStorage('taxonomy_term')->load($allowedTagId['target_id'])->getName();
      }
      $tags = $video->getSource()->getMetadata($video, 'tags');
      $published = FALSE;
      foreach ($tags as $tag) {
        foreach ($allowedTags as $allowedTag) {
          if ($tag == $allowedTag) {
            $published = TRUE;
            break;
          }
        }
      }
      if (!$published) {
        $event->unpublish();
      }
    }
  }

}
