<?php

namespace Drupal\yt_push\EventSubscriber;

use Drupal\yt_push\Event\YouTubePushEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\yt_push\Event\YouTubePublishEvent;

/**
 * Default event subscriber for the YouTube publish event.
 *
 * Determines publish state for the video going off to YouTube by the media
 * entity publish status.
 */
class YouTubePublishEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[YouTubePushEvents::PUBLISH][] = ['doPublishMedia'];

    return $events;
  }

  /**
   * Event handler.
   *
   * This default event handler will check the publish status of the media
   * entity, and mark the video for publish to YouTube based on that. If the
   * publish status of the media entity is TRUE, we publish to YouTube,
   * otherwise, we do not.
   *
   * @param \Drupal\yt_push\Event\YouTubePublishEvent $event
   *   YouTube publish event object.
   */
  public function doPublishMedia(YouTubePublishEvent $event) {

    // Ensure doPublish has not already been set to false.
    $video = $event->getMedia();
    if (!$video->isPublished()) {
      $event->unpublish();
    }
  }

}
