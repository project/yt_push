<?php

namespace Drupal\yt_push;

/**
 * Used for when there is a problem downloading a thumbnail from YouTube.
 */
class ThumbnailDownloadException extends \RuntimeException {
}
