<?php

namespace Drupal\yt_push\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\yt_push\Client\YouTubeClientFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;

/**
 * Controller callback to validate and save OAuth tokens.
 */
class OAuthTokenController implements ContainerInjectionInterface {
  use StringTranslationTrait;

  /**
   * The Entity Type Manager, used to load YouTube configuration.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The YouTube client factory.
   *
   * @var \Drupal\yt_push\Client\YouTubeClientFactory
   */
  protected $youTubeClientFactory;

  /**
   * The generator used to set the YouTube redirect URL.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * OAuthTokenController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager, used to load YouTube configuration.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The generator used to set the YouTube redirect URL.
   * @param \Drupal\yt_push\Client\YouTubeClientFactory $youtube_client_factory
   *   YouTube client factory object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, UrlGeneratorInterface $url_generator, YouTubeClientFactory $youtube_client_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->urlGenerator = $url_generator;
    $this->youTubeClientFactory = $youtube_client_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('url_generator'),
      $container->get('yt_push.youtube_client_factory')
    );
  }

  /**
   * Controller callback to authenticate an OAuth code.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The OAuth redirect request.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   An empty markup array, or a redirect if a destination was set.
   */
  public function onAuthenticate(Request $request) {
    $code = $request->get('code');
    // @todo Handle error case
    // https://developers.google.com/youtube/v3/guides/auth/server-side-web-apps#exchange-authorization-code
    try {
      $media_type_id = $this->getMediaTypeIdFromRequest($request);
      $types = $this->getMatchingMediaTypes($media_type_id);

      $redirect_url = $this->urlGenerator->generateFromRoute('yt_push.oauth_token', [], ['absolute' => TRUE]);

      $this->saveRefreshToken($types, $redirect_url, $code);
    }
    catch (\UnexpectedValueException $e) {
      $message = [
        '#markup' => sprintf('%s. The authentication code has not been saved.', $e->getMessage()),
      ];
      return $message;
    }

    // If a destination has been set, redirect to it.
    if ($state = $this->getStateFromRequest($request)) {
      $destination = $state['destination'];
      $url = Url::fromUserInput('/' . ltrim($destination, '/'), ['absolute' => TRUE])->toString();
      return new RedirectResponse($url);
    }

    // In general, all OAuth requests should specify the destination. If they
    // don't, we need to return a fallback render array (since all of our real
    // messages are set through \drupal_set_message().
    return [
      '#markup' => '',
    ];
  }

  /**
   * Validate and save a refresh token.
   *
   * @param \Drupal\media\MediaTypeInterface[] $media_types
   *   The media types to save the authorization code for.
   * @param string $redirect_url
   *   The redirect URL currently being redirected to.
   * @param string $code
   *   The OAuth code to validate and and use to generate the refresh token.
   */
  private function saveRefreshToken(array $media_types, $redirect_url, $code) {
    $client = $this->youTubeClientFactory->fromMediaType(reset($media_types));

    // While the authorization configuration contains an array of all redirect
    // URIs, the SDK only validates the first one unless we manually set it.
    $client->setRedirectUri($redirect_url);
    $credentials = $client->fetchAccessTokenWithAuthCode($code);

    // @todo DRACO-3733 Create a dedicated exception to cover for the SDK
    // downcasting errors to arrays.
    if (!empty($credentials['error'])) {
      throw new \RuntimeException($credentials['error_description']);
    }

    if (empty($credentials['refresh_token'])) {
      throw new \RuntimeException('No refresh token was returned');
    }

    // We now know $code is valid, so save it to all the given matching media
    // types.
    foreach ($media_types as $media_type) {
      $source = $media_type->getSource();
      $configuration = $source->getConfiguration();
      $configuration['refresh_token'] = $credentials['refresh_token'];
      $source->setConfiguration($configuration);
      $media_type->save();
      \drupal_set_message($this->t('%code was saved for the @type-link media type.', [
        '%code' => $code,
        '@type-link' => $media_type->toLink(NULL, 'edit-form')->toString(),
      ]));
    }
  }

  /**
   * Get matching media types.
   *
   * Return any media types which have a client id that matches the given media
   * type.
   *
   * @param string $target_media_type_id
   *   Media type id.
   *
   * @return \Drupal\media\MediaTypeInterface[]
   *   Return the list of media types that match the given media type id.
   */
  protected function getMatchingMediaTypes($target_media_type_id) {
    // Load all the YouTube media types. The one which corresponds to the passed
    // $media_type_id, is the target media type. We are interested in returning
    // a set of media types according to all media types which have the same
    // client id as the target media type. To avoid querying the database more
    // than once, we load all relevant media types including the target in one
    // shot, then group them by client id. We also track which of the set is the
    // target media type, for the purpose of identifying the group of media
    // types we wish to return.
    /** @var \Drupal\media\MediaTypeInterface[] $media_types */
    $media_types = $this->entityTypeManager->getStorage('media_type')
      ->loadByProperties(['source' => 'yt_push_field']);

    $json_decode = new JsonDecode(TRUE);
    $media_types_by_client_id = [];
    $target_client_id = NULL;
    foreach ($media_types as $media_type) {
      try {
        $client_id = $json_decode->decode($media_type->getSource()->getConfiguration()['client_id_json'], 'json');
        // Skip media types with empty or malformed client ids.
        if (empty($client_id['web']['client_id'])) {
          continue;
        }
        // Group the media type by it's client id.
        $client_id = $client_id['web']['client_id'];
        $media_types_by_client_id[$client_id][] = $media_type;
        // If this media type is the target media type, mark it's client id as
        // the target.
        if ($media_type->id() === $target_media_type_id) {
          $target_client_id = $client_id;
        }
      }
      catch (UnexpectedValueException $e) {
        // Throw away any JSON decoding error.
      }
    }

    // If we didn't find the target client id, that implies it doesn't exist.
    // If we don't have any media types that match the target client id, we also
    // fail to find a match. Explicitly throw an UnexpectedValueException to
    // flag this as an error state.
    if (!$target_client_id || empty($media_types_by_client_id[$target_client_id])) {
      throw new \UnexpectedValueException(sprintf('There are no YouTube media types matching the given media type id, %s.', $target_media_type_id));
    }
    // Return the list of target media types.
    return $media_types_by_client_id[$target_client_id] ?? [];
  }

  /**
   * Get the media type id from the state on the OAuth redirect request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The OAuth redirect request.
   *
   * @return string
   *   Media type id.
   *
   * @throws \UnexpectedValueException
   *   Thrown if the media type id is empty.
   */
  protected function getMediaTypeIdFromRequest(Request $request) {
    $state = $this->getStateFromRequest($request);
    if (empty($state['media_type_id'])) {
      throw new \UnexpectedValueException('Empty media type id on the OAuth redirect request.');
    }
    return $state['media_type_id'];
  }

  /**
   * Get the state values from the OAuth redirect request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The OAuth redirect request.
   *
   * @return array
   *   OAuth redirect request state value.
   *
   * @throws \UnexpectedValueException
   *   If either the state is onr present on the request, or we can't decode the
   *   state string as JSON, throw an UnexpectedValueException.
   */
  protected function getStateFromRequest(Request $request) {
    if (!$state = $request->get('state')) {
      throw new \UnexpectedValueException('State value on the OAuth redirect request is empty.');
    }

    try {
      $decoder = new JsonDecode(TRUE);
      return $decoder->decode($state, 'json');
    }
    catch (UnexpectedValueException $e) {
      throw new \UnexpectedValueException('Error decoding the state value on the OAuth redirect request.');
    }
  }

}
