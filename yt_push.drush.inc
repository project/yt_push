<?php

/**
 * @file
 * Drush commands.
 */

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\media\Entity\MediaType;

/**
 * Implements hook_drush_command().
 */
function yt_push_drush_command() {
  return [
    'config-add-youtube' => [
      'description' => 'Add a default YouTube media type pre-configured for the most common use case. As a last step, you\'ll need to edit the media type created and authenticate it against YouTube.',
      'examples' => [
        'drush config-add-youtube' => 'Add a default YouTube media type with typical options.',
        'drush config-add-youtube --external-video' => 'Add a default YouTube media type with typical options, configured for sourcing the video from external URL.',
      ],
      'options' => [
        'external-video' => [
          'description' => 'Configure the YouTube media type created to source videos from an external URL.',
        ],
      ],
      'aliases' => ['cay'],
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    ],
  ];
}

/**
 * Command hook for drush config-add-youtube.
 *
 * Adds a default YouTube media type pre-configured for the most common use
 * case.
 */
function drush_yt_push_config_add_youtube() {
  $plugin_id = 'yt_push_field';
  $yt_source = \Drupal::service('plugin.manager.media.source')->getDefinition($plugin_id);
  $is_external = drush_get_option('external-video', FALSE);

  // Create a source field. First check if we already have an appropriate field
  // storage.
  $source_storage = yt_push_create_media_field_storage($yt_source['id'], 'video_embed_field');
  // Create local video file field storage. First check if we already have an
  // appropriate field storage.
  if (!$is_external) {
    $local_video_storage = yt_push_create_media_field_storage('local_video_file', 'file');
  }
  else {
    $external_video_storage = yt_push_create_media_field_storage('external_video_url', 'string');
  }
  // Create a description field storage. First check if we already have an
  // appropriate field storage.
  $description_storage = yt_push_create_media_field_storage('description', 'string_long');
  // Create a taxonomy term entity reference. Only bother doing this if there is
  // a Tags vocabulary that we can use. Avoid going to all the trouble of
  // creating a custom vocabulary.
  $tags_storage = NULL;

  $tags_storage = yt_push_create_media_field_storage('tags', 'entity_reference');
  $tags_storage->setSetting('target_type', 'taxonomy_term')
    ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
    ->save();

  // Create privacy field storage.
  $privacy_storage = yt_push_create_media_field_storage('privacy', 'list_string', [
    'allowed_values' => [
      'unlisted' => 'Unlisted',
      'private' => 'Private',
      'public' => 'Public',
    ],
  ]);

  // Create the media type itself.
  $media_type_values = [
    'id' => yt_push_get_unique_media_type_id(),
    'label' => 'YouTube',
    'description' => 'Upload video to YouTube and store a link to the YouTube video.',
    'source' => $yt_source['id'],
    'source_configuration' => [
      'source_field' => $source_storage->getName(),
      'video_source_field' => $is_external ? $external_video_storage->getName() : $local_video_storage->getName(),
    ],
    'field_map' => [
      'description' => $description_storage->getName(),
      'privacy_status_field' => $privacy_storage->getName(),
    ],
  ];
  if ($tags_storage) {
    $media_type_values['field_map']['tags'] = $tags_storage->getName();
  }
  $media_type = MediaType::create($media_type_values);
  $media_type->save();

  // Create the YouTube field.
  $field = FieldConfig::create([
    'field_storage' => $source_storage,
    'bundle' => $media_type->id(),
    'label' => $yt_source['label'],
    'required' => FALSE,
    'settings' => [
      'allowed_providers' => [
        'youtube' => 'youtube',
      ],
    ],
  ]);
  $field->save();

  if ($is_external) {
    // Create field for the external video URL.
    $field = FieldConfig::create([
      'field_storage' => $external_video_storage,
      'bundle' => $media_type->id(),
      'label' => t('External video URL'),
      'required' => TRUE,
      'description' => t('Enter the URL of an external video that should be uploaded to YouTube. HTTP and HTTPS are supported.'),
    ]);
    $field->save();
  }
  else {
    // Create field for the local video file.
    $field = FieldConfig::create([
      'field_storage' => $local_video_storage,
      'bundle' => $media_type->id(),
      'label' => t('Local video file'),
      'required' => TRUE,
      'settings' => [
        'file_extensions' => 'mov mp4 mpeg4 avi wmv flv 3gp mpegps webm',
      ],
      'description' => t('Upload a file, which will in turn be uploaded to YouTube.'),
    ]);
    $field->save();
  }

  // Create a field for the description.
  $field = FieldConfig::create([
    'field_storage' => $description_storage,
    'bundle' => $media_type->id(),
    'label' => t('Description'),
    'required' => FALSE,
    'description' => t('Description of the video for YouTube.'),
  ]);
  $field->save();

  // Create the field for the tags if we were able to create the storage.
  if ($tags_storage) {
    $field = FieldConfig::create([
      'field_storage' => $tags_storage,
      'bundle' => $media_type->id(),
      'label' => t('Tags'),
      'required' => FALSE,
      'settings' => [
        'handler_settings' => [
          'target_bundles' => [
            'tags' => 'tags',
          ],
          'auto_create' => TRUE,
        ],
      ],
    ]);
    $field->save();
  }

  // Create the field for privacy.
  $field = FieldConfig::create([
    'field_storage' => $privacy_storage,
    'bundle' => $media_type->id(),
    'label' => t('Privacy'),
    'required' => FALSE,
    'description' => t('Privacy status for the video on YouTube.'),
  ]);
  $field->save();

  // Create an entity form display for the YouTube media type.
  $form_display = EntityFormDisplay::create([
    'targetEntityType' => 'media',
    'bundle' => $media_type->id(),
    'mode' => 'default',
    'status' => TRUE,
  ]);
  $form_display->setComponent('name', ['weight' => 0]);
  if ($is_external) {
    $form_display->setComponent($external_video_storage->getName(), ['weight' => 1]);
  }
  else {
    $form_display->setComponent($local_video_storage->getName(), ['weight' => 1]);
  }
  $form_display->setComponent($source_storage->getName(), ['weight' => 2]);
  $form_display->setComponent($description_storage->getName(), ['weight' => 3]);
  if ($tags_storage) {
    $form_display->setComponent($tags_storage->getName(), [
      'weight' => 4,
      'type' => 'entity_reference_autocomplete_tags',
    ]);
  }
  $form_display->setComponent($privacy_storage->getName(), ['weight' => 5]);
  $form_display->save();

  // Create a entity view display for the YouTube media type.
  $view_display = EntityViewDisplay::create([
    'targetEntityType' => 'media',
    'bundle' => $media_type->id(),
    'mode' => 'default',
    'status' => TRUE,
  ]);
  $view_display->setComponent('thumbnail', [
    'weight' => 0,
    'region' => 'content',
    'label' => 'above',
  ]);
  $view_display->setComponent($source_storage->getName(), [
    'weight' => 1,
    'region' => 'content',
    'label' => 'above',
  ]);
  $view_display->setComponent($description_storage->getName(), [
    'weight' => 2,
    'region' => 'content',
    'label' => 'above',
  ]);
  if ($tags_storage) {
    $view_display->setComponent($tags_storage->getName(), [
      'weight' => 3,
      'region' => 'content',
      'label' => 'above',
    ]);
  }
  $view_display->setComponent($privacy_storage->getName(), [
    'weight' => 4,
    'region' => 'content',
    'label' => 'above',
  ]);
  $view_display->removeComponent('created');
  $view_display->removeComponent('uid');
  $view_display->save();

  drush_print(sprintf('Successfully added a YouTube media type. Please enter your YouTube credentials and authenticate against YouTube here: %s', $media_type->toUrl('edit-form')->setAbsolute()->toString()));
}

/**
 * Generate a unique media type id for YouTube.
 *
 * @return string
 *   A unique media type id for a YouTube media type.
 */
function yt_push_get_unique_media_type_id() {
  $storage = \Drupal::entityTypeManager()->getStorage('media_type');
  $media_type_ids = array_keys($storage->loadMultiple());

  $youtube_id = 'youtube';
  $i = 1;
  while (in_array($youtube_id, $media_type_ids)) {
    $youtube_id = sprintf('youtube_%d', $i++);
  }
  return $youtube_id;
}

/**
 * Helper function to create field storage for a media type entity.
 *
 * @param string $name
 *   Name of the field. Will be prefixed with 'field_media_'.
 * @param string $type
 *   Field type.
 * @param array $settings
 *   (optional) Settings for the field storage.
 *
 * @return \Drupal\field\Entity\FieldStorageConfig
 *   Existing or created field storage.
 */
function yt_push_create_media_field_storage($name, $type, array $settings = NULL) {
  $field_storage = FieldStorageConfig::loadByName('media', 'field_media_' . $name);
  if (!$field_storage) {
    $values = [
      'entity_type' => 'media',
      'field_name' => 'field_media_' . $name,
      'type' => $type,
    ];
    if ($settings) {
      $values['settings'] = $settings;
    }
    $field_storage = FieldStorageConfig::create($values);
    $field_storage->save();
  }
  return $field_storage;
}
