# YouTube Push

Upload videos to YouTube and save the result using
[Video Embed Field](https://www.drupal.org/project/video_embed_field).

All development and issue tracking occurs on the
[GitHub repository](https://github.com/turnerlabs/yt_push).

## Installation

This module contains composer dependencies. It's recommended that you download
the module with Composer. Install the module as any Drupal module.

This module comes with a Drush command that helps you to quickly and easily
add a YouTube media type pre-configured with the most common options. To use it,
simply run:

```
$ drush config-add-youtube
```

The drush command will create a new media type named YouTube. Follow the link in
the command output to finish the configuration by entering your credentials and
authenticating with YouTube.

## Configuration

### Privacy status

With some configuration, you can dynamically set the privacy status on uploaded
videos. First create a new field on your YouTube media type to store the privacy
status. YouTube Push module will attempt to read the value of your field as a
string, with valid values of 'unlisted', 'private', and 'public'. If it can't,
it will assume your field is a boolean, where a truthy value means 'private'
and a falsy value means 'public'. Thus the field you create will typically be
either a checkbox field labeled 'Private video', or a select field with options
for 'unlisted', 'private', and 'public'. Create the desired field at
Administration > Structure > Media types > Edit <media_type> > Manage fields.

Note that you can effectively set the privacy status to a default by setting the
desired default value in the field settings, and then hiding the field from the
form display.

Once you have your privacy status field setup, the last step is to edit your
media type and select the field you created for "Field which stores the privacy
status of the video on YouTube" in the field mapping. At this point when you
create a new video to upload to YouTube, the value for your privacy status field
is reflected in the video uploaded to YouTube.

Note that if you do not configure a field for privacy status, we default to
public.

### Core media bug when saving duplicate media type

There is a bug in Drupal core < 8.5 when you save a new media type with the same
machine name as one that already exists. It produces the following WSOD error:

```
Uncaught PHP Exception Drupal\\Core\\Entity\\EntityStorageException:
"'media_type' entity with ID 'image' already exists." at
/.../core/lib/Drupal/Core/Entity/EntityStorageBase.php line 425, referer:
http://example.com/admin/structure/media/add
```

The workaround is to be careful about not saving more than one media type with
the same machine name. The long term solution is to update to Drupal 8.5 (not
yet available at time of writing), or consult this issue to patch Drupal core:

https://www.drupal.org/project/drupal/issues/2932226
