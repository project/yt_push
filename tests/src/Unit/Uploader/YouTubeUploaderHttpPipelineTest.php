<?php

namespace Drupal\Tests\yt_push\Unit\Uploader;

use Drupal\Tests\UnitTestCase;
use Drupal\yt_push\Uploader\YouTubeUploaderHttpPipeline;
use Google_Http_MediaFileUpload;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\Promise;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;
use ReflectionMethod;

/**
 * Tests for YouTubeUploaderHttpPipeline class.
 *
 * @coversDefaultClass \Drupal\yt_push\Uploader\YouTubeUploaderHttpPipeline
 *
 * @group yt_push
 */
class YouTubeUploaderHttpPipelineTest extends UnitTestCase {

  /**
   * Mock HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Mock HEAD response for the video URI.
   *
   * Important for setting the file size of the video upload, by inspecting the
   * Content-Length header.
   *
   * @var \Psr\Http\Message\ResponseInterface
   */
  protected $videoHeadResponse;

  /**
   * The mock stream response for the external video fetch.
   *
   * @var \Psr\Http\Message\StreamInterface
   */
  protected $videoBody;

  /**
   * YouTube Uploader Http pipeline that we are testing.
   *
   * @var \Drupal\yt_push\Uploader\YouTubeUploaderHttpPipeline
   */
  protected $youTubeUploaderHttpPipeline;

  /**
   * Video response interface.
   *
   * @var \Psr\Http\Message\ResponseInterface
   */
  protected $videoResponse;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // Mock httpClient. It will be asked for:
    // - head(): Return a \Psr\Http\Message\ResponseInterface
    //   - getHeader(): Return a numerically indexed array where the first index
    //     is an integer indicating the number of bytes.
    // - getAsync(): Return a \GuzzleHttp\Promise\PromiseInterface object.
    //   Probably one that is resolved with a
    //   \Psr\Http\Message\ResponseInterface object that we want to control:
    //   - getBody()
    //     - read()
    //     - eof()
    // Need to get all the methods of ResponseInterface to be able to get a
    // mock that we can replace just one method on.
    $class = new \ReflectionClass(ResponseInterface::class);
    $response_methods = array_filter(array_map(function (ReflectionMethod $method) {
      return $method->isAbstract() ? $method->getName() : NULL;
    }, $class->getMethods()));
    $class = new \ReflectionClass(StreamInterface::class);
    $stream_methods = array_filter(array_map(function (ReflectionMethod $method) {
      return $method->isAbstract() ? $method->getName() : NULL;
    }, $class->getMethods()));

    $this->videoHeadResponse = $this->getMockBuilder(ResponseInterface::class)
      ->disableOriginalConstructor()
      ->setMethods($response_methods)
      ->getMock();
    $this->videoBody = $this->getMockBuilder(StreamInterface::class)
      ->disableOriginalConstructor()
      ->setMethods($stream_methods)
      ->getMock();
    $this->videoResponse = $this->getMockBuilder(ResponseInterface::class)
      ->disableOriginalConstructor()
      ->setMethods($response_methods)
      ->getMock();
    $this->videoResponse->method('getBody')
      ->willReturn($this->videoBody);
    $promise = new Promise();
    $promise->resolve($this->videoResponse);

    $this->httpClient = $this->getMockBuilder(Client::class)
      ->disableOriginalConstructor()
      ->setMethods(['head', 'getAsync'])
      ->getMock();
    $this->httpClient->method('head')
      ->willReturn($this->videoHeadResponse);
    $this->httpClient->method('getAsync')
      ->willReturn($promise);

    /** @var \Psr\Http\Message\UriInterface $video_uri */
    $video_uri = $this->createMock(UriInterface::class);
    $this->youTubeUploaderHttpPipeline = new YouTubeUploaderHttpPipeline($video_uri, $this->httpClient);
  }

  /**
   * Test the upload method.
   */
  public function testUpload() {
    // Set a 4MB file size.
    $video_size = 1024 * 1024 * 4;
    $total_bytes_received = 0;

    $this->videoHeadResponse->method('getHeader')
      ->willReturn([$video_size]);
    $google_upload_mock = $this->getMockBuilder(Google_Http_MediaFileUpload::class)
      ->disableOriginalConstructor()
      ->setMethods(['setFileSize', 'nextChunk'])
      ->getMock();

    // We expect that the video size is set from the HEAD response to the video.
    $google_upload_mock->expects($this->once())
      ->method('setFileSize')
      ->with($this->equalTo($video_size));

    // This is the main assertion that we are interested in. We want to ensure
    // that the number of bytes that we pass to YouTube is at least 262144 or a
    // multiple thereof.
    $google_upload_mock->method('nextChunk')
      ->with($this->callback(function ($chunk) {
        // Verify that the $chunk passed has a length that is a multiple of
        // 262144 bytes (0.25 Mebibyte).
        return strlen($chunk) % 262144 == 0;
      }));

    // Simulate a real HTTP response by returning a random number of bytes
    // until all bytes are in hand.
    $this->videoBody->method('read')
      ->will($this->returnCallback(function () use ($video_size, &$total_bytes_received) {
        // Choose a random number of byes between 1024 and 1024 * 1024 bytes.
        // This simulates real world read lengths.
        $length = rand(1024, 1024 * 1024);
        // Return the minimum of the number of bytes left to read in the
        // simulated video stream, or $length.
        $read_length = min($length, $video_size - $total_bytes_received);
        $total_bytes_received += $read_length;
        return str_pad('', $read_length, 'o');
      }));

    // Mock eof to return TRUE once the number of bytes we have received
    // surpasses the total.
    $this->videoBody->method('eof')
      ->will($this->returnCallback(function () use ($video_size, &$total_bytes_received) {
        return $total_bytes_received == $video_size;
      }));

    // Finally run the upload method. All assertions are coded into the mocks
    // above.
    $this->youTubeUploaderHttpPipeline->upload($google_upload_mock, 1024 * 1024);
  }

}
