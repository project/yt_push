<?php

namespace Drupal\Tests\yt_push\Unit\Client;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Http\ClientFactory;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\yt_push\Client\YouTubeClientFactory;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Serializer\Encoder\JsonDecode;

/**
 * Tests for YouTubeClientFactoryTest class.
 *
 * @coversDefaultClass \Drupal\yt_push\Client\YouTubeClientFactory
 *
 * @group yt_push
 */
class YouTubeClientFactoryTest extends UnitTestCase {

  /**
   * YouTube client factory under test.
   *
   * @var \Drupal\yt_push\Client\YouTubeClientFactory
   */
  protected $youTubeClientFactory;

  /**
   * Drupal client factory mock.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $drupalClientFactory;

  /**
   * Entity type manager mock.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // Build up a mock for the HTTP client that Google_Client uses so that it
    // will return a relevant response for the test we want to run.
    $response = $this->getMockBuilder(ResponseInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $response->method('getBody')
      ->willReturn('{"access_token": "access_token", "refresh_token": "refresh_token"}');

    $client = $this->getMockBuilder(ClientInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $client->method('send')
      ->willReturn($response);

    $this->drupalClientFactory = $this->getMockBuilder(ClientFactory::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->drupalClientFactory->method('fromOptions')
      ->willReturn($client);

    $storage = $this->getMock(EntityStorageInterface::class);
    $this->entityTypeManager = $this->getMockBuilder(EntityTypeManager::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->entityTypeManager->method('getStorage')
      ->willReturn($storage);

    $this->youTubeClientFactory = new YouTubeClientFactory($this->drupalClientFactory, $this->entityTypeManager);
  }

  /**
   * Test the fromOptions method.
   *
   * @param array $http_config
   *   Http config to use with the fromOptions method.
   * @param array $google_auth_config
   *   Google auth configs. We expect to see these reflected in the
   *   Google_Client object we get back.
   *
   * @covers ::fromOptions
   * @covers ::getDefaultGoogleAuthConfig
   *
   * @dataProvider factoryMethodDataProvider
   */
  public function testFromOptions(array $http_config, array $google_auth_config) {
    $client = $this->youTubeClientFactory->fromOptions($http_config, $google_auth_config);
    $this->assertClientProperties($google_auth_config, $client);
  }

  /**
   * Test the fromMediaType method.
   *
   * @param array $http_config
   *   Http config to use with the fromOptions method.
   * @param array $google_auth_config
   *   Google auth configs. We expect to see these reflected in the
   *   Google_Client object we get back.
   *
   * @covers ::fromMediaType
   * @covers ::fromOptions
   * @covers ::getDefaultGoogleAuthConfig
   *
   * @dataProvider factoryMethodDataProvider
   */
  public function testFromMediaType(array $http_config, array $google_auth_config) {
    $media_source = $this->getMockBuilder(MediaSourceInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $media_source->method('getConfiguration')
      ->willReturn($google_auth_config);
    $media_type = $this->getMockBuilder(MediaTypeInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $media_type->method('getSource')
      ->willReturn($media_source);

    $client = $this->youTubeClientFactory->fromMediaType($media_type, $http_config);
    $this->assertClientProperties($google_auth_config, $client);
  }

  /**
   * Test the fromMedia method.
   *
   * @param array $http_config
   *   Http config to use with the fromOptions method.
   * @param array $google_auth_config
   *   Google auth configs. We expect to see these reflected in the
   *   Google_Client object we get back.
   *
   * @covers ::fromMedia
   * @covers ::fromOptions
   * @covers ::getDefaultGoogleAuthConfig
   *
   * @dataProvider factoryMethodDataProvider
   */
  public function testFromMedia(array $http_config, array $google_auth_config) {
    $media_source = $this->getMockBuilder(MediaSourceInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $media_source->method('getConfiguration')
      ->willReturn($google_auth_config);
    $media = $this->getMockBuilder(MediaInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $media->method('getSource')
      ->willReturn($media_source);

    $client = $this->youTubeClientFactory->fromMedia($media, $http_config);
    $this->assertClientProperties($google_auth_config, $client);
  }

  /**
   * Assert google client properties.
   *
   * Assert that properties of the Google_Client were set correctly by the
   * factory.
   *
   * @param array $google_auth_config
   *   Google auth configs. We expect to see these reflected in the
   *   Google_Client object we get back.
   * @param \Google_Client $client
   *   Google_Client object in which to check for the google auth configs.
   */
  protected function assertClientProperties(array $google_auth_config, \Google_Client $client) {
    $this->assertInstanceOf(ClientInterface::class, $client->getHttpClient());

    foreach ($google_auth_config as $key => $value) {
      switch ($key) {
        case 'client_id_json':
          $decoder = new JsonDecode(TRUE);
          $decoded = $decoder->decode($value, 'json');
          $this->assertEquals($decoded['web']['client_id'], $client->getClientId());
          $this->assertEquals($decoded['web']['client_secret'], $client->getClientSecret());
          break;

        case 'refresh_token':
          $this->assertEquals($value, $client->getRefreshToken());
          break;

        case 'redirect_uri':
          $this->assertEquals($value, $client->getRedirectUri());
          break;
      }
    }
  }

  /**
   * Data provider for various factory methods.
   *
   * @return array
   *   Array of arguments for the factory methods.
   */
  public function factoryMethodDataProvider() {
    return [
      [
        [],
        [],
      ],
      [
        [],
        [
          'client_id_json' => '{"web":{"client_id":"happy_fun_time_client_id","project_id":"turner-asset-ingestion","auth_uri":"https://accounts.google.com/o/oauth2/auth","token_uri":"https://accounts.google.com/o/oauth2/token","auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs","client_secret":"shhhh_its_a_secret","redirect_uris":["http://localhost/_yt-push/oauth-token","http://local.draco.inturner.io/_yt-push/oauth-token"]}}',
          'refresh_token' => 'refresh_token',
          'redirect_uri' => 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
        ],
      ],
    ];
  }

}
