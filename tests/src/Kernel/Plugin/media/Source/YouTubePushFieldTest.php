<?php

namespace Drupal\Tests\yt_push\Kernel\Plugin\media\Source;

use Drupal\Core\Queue\QueueInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\Tests\media\Kernel\MediaKernelTestBase;
use Drupal\Tests\TestFileCreationTrait;
use Drupal\yt_push\Plugin\media\Source\YouTubePushField;
use Drupal\yt_push\Plugin\QueueWorker\Exception\InvalidSourceVideoException;

/**
 * Tests the YouTubePushField media source plugin.
 *
 * @coversDefaultClass \Drupal\yt_push\Plugin\media\Source\YouTubePushField
 *
 * @group yt_push
 */
class YouTubePushFieldTest extends MediaKernelTestBase {
  use TestFileCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'simpletest',
    'video_embed_field',
    'video_embed_media',
    'yt_push',
    'taxonomy',
    'text',
  ];

  /**
   * Media type under test.
   *
   * @var \Drupal\media\MediaTypeInterface
   */
  protected $youTubeMediaType;

  /**
   * The folder where thumbnails are stored.
   *
   * @var string
   */
  protected $thumbsFolder = 'video_thumbnails';

  /**
   * A mock queue.
   *
   * @var \PHPUnit_Framework_MockObject_MockObject
   */
  protected $queue;

  /**
   * Strict configuration settings.
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->youTubeMediaType = $this->createMediaType('yt_push_field');
  }

  /**
   * Test the getMetadata method for a new media entity.
   */
  public function testDefaultNewMetadataThumbnailUri() {
    // We also want to ensure that in the case where the media entity has not
    // yet been saved (and thus doesn't have an id), we don't queue a thumbnail
    // download.
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
    ]);

    // We expect that, since the video isn't available yet, and the media entity
    // has not been saved, it will not queue the thumbnail for download.
    $queue = $this->getMockBuilder(QueueInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $queue->expects($this->never())
      ->method('createItem');
    /** @var \Drupal\yt_push\Plugin\media\Source\YouTubePushField $source */
    $source = $media->getSource();
    $this->assertInstanceOf(YouTubePushField::class, $source);
    $source->setThumbnailDownloadQueue($queue);

    $expected = $this->config('media.settings')->get('icon_base_uri') . '/' . $source->getPluginDefinition()['default_thumbnail_filename'];
    $this->assertEquals($expected, $source->getMetadata($media, 'thumbnail_uri'));
  }

  /**
   * Test the getMetadata method for an existing media entity.
   */
  public function testDefaultUpdateMetadataThumbnailUri() {
    // We also want to ensure that in the case where the media entity has not
    // yet been saved (and thus doesn't have an id), we don't queue a thumbnail
    // download.
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
    ]);
    $media->save();

    // Now that the media entity is saved and has an id, we expect that the
    // thumbnail download will be queued.
    $queue = $this->getMockBuilder(QueueInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $queue->expects($this->once())
      ->method('createItem')
      ->with($this->equalTo(['entity_id' => $media->id()]));
    /** @var \Drupal\yt_push\Plugin\media\Source\YouTubePushField $source */
    $source = $media->getSource();
    $this->assertInstanceOf(YouTubePushField::class, $source);
    $source->setThumbnailDownloadQueue($queue);

    $expected = $this->config('media.settings')->get('icon_base_uri') . '/' . $source->getPluginDefinition()['default_thumbnail_filename'];
    $this->assertEquals($expected, $source->getMetadata($media, 'thumbnail_uri'));
  }

  /**
   * Test that a downloaded thumbnail from YouTube is returned.
   */
  public function testExistingMetadataThumbnailUri() {
    $field_name = $this->youTubeMediaType->getSource()->getSourceFieldDefinition($this->youTubeMediaType)->getName();
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
      $field_name => [
        [
          'value' => 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
        ],
      ],
    ]);

    // Create the thumbnail as if it has already been downloaded from YouTube.
    $thumbs_directory = file_default_scheme() . '://' . $this->thumbsFolder;
    $test_thumbnail = $thumbs_directory . '/dQw4w9WgXcQ.jpg';
    file_prepare_directory($thumbs_directory, FILE_CREATE_DIRECTORY);
    file_put_contents($test_thumbnail, 'nonsense content');

    // We should be back the thumbnail URI for the one on the file system that
    // we just created.
    $this->assertEquals($test_thumbnail, $media->getSource()->getMetadata($media, 'thumbnail_uri'));
  }

  /**
   * Test getVideoSourceUri when the video source field isn't defined.
   */
  public function testGetVideoSourceUriMissingSourceField() {
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
    ]);
    /** @var \Drupal\yt_push\Plugin\media\Source\YouTubePushField $source */
    $source = $this->youTubeMediaType->getSource();

    $this->setExpectedException(InvalidSourceVideoException::class, 'The given media does not have a configured video source field. Edit the settings for your YouTube media type and select a Video source field.');
    $source->getVideoSourceUri($media);
  }

  /**
   * Test getVideoSourceUri when the video source field missing on entity type.
   *
   * This situation is rare, but it would be where somehow the field storage
   * exists and the source configuration pointed to that field, but the field
   * is not defined for the media type.
   */
  public function testGetVideoSourceUdiMissingOnMediaType() {
    $field_storage = FieldStorageConfig::create([
      'entity_type' => 'media',
      'field_name' => 'field_media_local_video_file',
      'type' => 'file',
    ]);
    $field_storage->save();
    /** @var \Drupal\yt_push\Plugin\media\Source\YouTubePushField $source */
    $source = $this->youTubeMediaType->getSource();
    $config = $source->getConfiguration();
    $config['video_source_field'] = $field_storage->getName();
    $this->youTubeMediaType->set('source_configuration', $config)
      ->save();
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
    ]);

    $this->setExpectedException(InvalidSourceVideoException::class, 'The configured video source field does not exist on the media type.');
    $source->getVideoSourceUri($media);
  }

  /**
   * Test getVideoSourceUri when the video source field is empty.
   */
  public function testGetVideoSourceUriEmptyValue() {
    $field = $this->addLocalVideoSourceField();
    /** @var \Drupal\yt_push\Plugin\media\Source\YouTubePushField $source */
    $source = $this->youTubeMediaType->getSource();
    $config = $source->getConfiguration();
    $config['video_source_field'] = $field->getName();
    $this->youTubeMediaType->set('source_configuration', $config)
      ->save();
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
    ]);

    $this->setExpectedException(InvalidSourceVideoException::class, 'The video source field is empty for the given media.');
    $source->getVideoSourceUri($media);
  }

  /**
   * Test getVideoSourceUri.
   *
   * When there is a value for the video file that doesn't exist.
   */
  public function testGetVideoSourceUriLocalVideoMissing() {
    $field = $this->addLocalVideoSourceField();
    /** @var \Drupal\yt_push\Plugin\media\Source\YouTubePushField $source */
    $source = $this->youTubeMediaType->getSource();
    $config = $source->getConfiguration();
    $config['video_source_field'] = $field->getName();
    $this->youTubeMediaType->set('source_configuration', $config)
      ->save();
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
      $field->getName() => [
        [
          'target_id' => 42,
        ],
      ],
    ]);
    $this->setExpectedException(InvalidSourceVideoException::class, 'There is no attached video file to upload.');
    $source->getVideoSourceUri($media);
  }

  /**
   * Test getVideoSourceUri.
   *
   * When the video source is configured for external URL and the value of that
   * field is not a URL.
   */
  public function testGetVideoSourceUriExternalUriInvalid() {
    $field = $this->addExternalUriVideoSourceField();
    /** @var \Drupal\yt_push\Plugin\media\Source\YouTubePushField $source */
    $source = $this->youTubeMediaType->getSource();
    $config = $source->getConfiguration();
    $config['video_source_field'] = $field->getName();
    $this->youTubeMediaType->set('source_configuration', $config)
      ->save();
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
      $field->getName() => [
        [
          'value' => 'i can\'t imagine this is any kind of url',
        ],
      ],
    ]);
    $this->setExpectedException(InvalidSourceVideoException::class, 'The video source field was interpreted as an external URL and found to either be empty or invalid.');
    $source->getVideoSourceUri($media);
  }

  /**
   * Test the happy path for getVideoSourceUri and local video file.
   */
  public function testGetVideoSourceUriLocalVideo() {
    $field = $this->addLocalVideoSourceField();
    /** @var \Drupal\yt_push\Plugin\media\Source\YouTubePushField $source */
    $source = $this->youTubeMediaType->getSource();
    $config = $source->getConfiguration();
    $config['video_source_field'] = $field->getName();
    $this->youTubeMediaType->set('source_configuration', $config)
      ->save();

    $files = $this->getTestFiles('binary');
    $file = File::create([
      'uri' => $files[0]->uri,
      'uid' => 1,
    ]);
    $file->save();
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
      $field->getName() => [
        [
          'target_id' => $file->id(),
        ],
      ],
    ]);

    $this->assertEquals($file->getFileUri(), $source->getVideoSourceUri($media)->__toString());
  }

  /**
   * Test the happy path for getVideoSourceUri and local video file.
   */
  public function testGetVideoSourceExternalUri() {
    $field = $this->addExternalUriVideoSourceField();
    /** @var \Drupal\yt_push\Plugin\media\Source\YouTubePushField $source */
    $source = $this->youTubeMediaType->getSource();
    $config = $source->getConfiguration();
    $config['video_source_field'] = $field->getName();
    $this->youTubeMediaType->set('source_configuration', $config)
      ->save();

    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
      $field->getName() => [
        [
          'value' => 'http://techslides.com/demos/sample-videos/small.webm',
        ],
      ],
    ]);

    $this->assertEquals('http://techslides.com/demos/sample-videos/small.webm', $source->getVideoSourceUri($media)->__toString());
  }

  /**
   * Helper function to create a local video source field.
   *
   * @return \Drupal\field\Entity\FieldConfig
   *   Field config entity for the field created.
   */
  protected function addLocalVideoSourceField() {
    $field_storage = FieldStorageConfig::create([
      'entity_type' => 'media',
      'field_name' => 'field_media_local_video_file',
      'type' => 'file',
    ]);
    $field_storage->save();

    // Create field for the local video file.
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => $this->youTubeMediaType->id(),
      'label' => t('Local video file'),
      'required' => TRUE,
      'settings' => [
        'file_extensions' => 'mov mp4 mpeg4 avi wmv flv 3gp mpegps webm',
      ],
    ]);
    $field->save();
    return $field;
  }

  /**
   * Helper function to create a local video source uri field.
   *
   * @return \Drupal\field\Entity\FieldConfig
   *   Field config entity for the field created.
   */
  protected function addExternalUriVideoSourceField() {
    $field_storage = FieldStorageConfig::create([
      'entity_type' => 'media',
      'field_name' => 'field_media_external_video_url',
      'type' => 'string',
    ]);
    $field_storage->save();

    // Create field for the local video file.
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => $this->youTubeMediaType->id(),
      'label' => t('External video URL'),
      'required' => TRUE,
    ]);
    $field->save();
    return $field;
  }

}
