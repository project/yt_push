<?php

namespace Drupal\Tests\yt_push\Kernel\EventSubscriber;

use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\Tests\yt_push\Kernel\Plugin\media\Source\YouTubePushFieldTest;
use Drupal\yt_push\Event\YouTubePublishEvent;
use Drupal\yt_push\EventSubscriber\YouTubePublishEventSubscriber;

/**
 * Class YouTubePublishEventSubscriberTest.
 *
 * @package Drupal\Tests\yt_push\Kernel\EventSubscriber
 */
class YouTubePublishEventSubscriberTest extends YouTubePushFieldTest {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'simpletest',
    'video_embed_field',
    'video_embed_media',
    'yt_push',
    'taxonomy',
    'text',
  ];

  /**
   * Media type under test.
   *
   * @var \Drupal\media\MediaTypeInterface
   */
  protected $youTubeMediaType;

  /**
   * Tests the doPublishMedia method of YouTubePublishEventSubscriber.
   */
  public function testDoPublishMedia() {
    $subscriber = new YouTubePublishEventSubscriber();

    $videoField = $this->addLocalVideoSourceField();

    $files = $this->getTestFiles('binary');
    $file = File::create([
      'uri' => $files[0]->uri,
      'uid' => 1,
    ]);

    // Not published - fail.
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
      'status' => FALSE,
      $videoField->getName() => [
        [
          'target_id' => $file->id(),
        ],
      ],
    ]);
    $event = new YouTubePublishEvent($media);
    $subscriber->doPublishMedia($event);
    $this->assertFalse($event->checkPublish());

    // Published - pass.
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
      'status' => TRUE,
      $videoField->getName() => [
        [
          'target_id' => $file->id(),
        ],
      ],
    ]);
    $event = new YouTubePublishEvent($media);
    $subscriber->doPublishMedia($event);
    $this->assertTrue($event->checkPublish());
  }

}
