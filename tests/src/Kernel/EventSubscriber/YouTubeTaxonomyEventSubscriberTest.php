<?php

namespace Drupal\Tests\yt_push\Kernel\EventSubscriber;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\yt_push\Kernel\Plugin\media\Source\YouTubePushFieldTest;
use Drupal\yt_push\Event\YouTubePublishEvent;
use Drupal\yt_push\EventSubscriber\YouTubeTaxonomyEventSubscriber;

/**
 * Class YouTubeTaxonomyEventSubscriberTest.
 *
 * @package Drupal\Tests\yt_push\Kernel\EventSubscriber
 */
class YouTubeTaxonomyEventSubscriberTest extends YouTubePushFieldTest {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'simpletest',
    'video_embed_field',
    'video_embed_media',
    'yt_push',
    'taxonomy',
    'text',
    'field',
    'node',
    'filter',
  ];

  /**
   * Media type under test.
   *
   * @var \Drupal\media\MediaTypeInterface
   * The YouTube media type
   */
  protected $youTubeMediaType;

  /**
   * A test vocabulary.
   *
   * @var \Drupal\taxonomy\Entity\Vocabulary
   * A test vocabulary.
   */
  protected $fruits;

  /**
   * A test vocabulary.
   *
   * @var \Drupal\taxonomy\Entity\Vocabulary
   * A test vocabulary.
   */
  protected $colours;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->strictConfigSchema = FALSE;
    $this->installConfig('field');
    $this->installConfig('taxonomy');
    $this->installEntitySchema('taxonomy_term');
    $this->initializeVocabularies();
    $this->createTagField();
  }

  /**
   * Tests the doPublishMedia function of YouTubeTaxonomyEventSubscriber.
   */
  public function testDoPublishMedia() {
    $subscriber = new YouTubeTaxonomyEventSubscriber();

    $videoField = $this->addLocalVideoSourceField();

    $files = $this->getTestFiles('binary');
    $file = File::create([
      'uri' => $files[0]->uri,
      'uid' => 1,
    ]);

    // No whitelisted vocabulary or taxonomy, no tags - pass.
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
      'status' => TRUE,
      $videoField->getName() => [
        [
          'target_id' => $file->id(),
        ],
      ],
    ]);
    $event = new YouTubePublishEvent($media, TRUE);
    $subscriber->doPublishMedia($event);
    $this->assertTrue($event->checkPublish());

    // No whitelisted vocabulary or terms, some tags - pass.
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
      'status' => TRUE,
      $videoField->getName() => [
        [
          'target_id' => $file->id(),
        ],
      ],
      'field_media_tags' => [
        Term::load(1),
      ],
    ]);
    $media->save();
    $event = new YouTubePublishEvent($media, TRUE);
    $subscriber->doPublishMedia($event);
    $this->assertTrue($event->checkPublish());

    // Whitelisted vocabulary but no whitelisted terms, correct vocabulary-pass.
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
      'status' => TRUE,
      $videoField->getName() => [
        [
          'target_id' => $file->id(),
        ],
      ],
      'field_media_tags' => [
        Term::load(5), Term::load(2),
      ],
    ]);
    $media->getSource()->setConfiguration([
      'taxonomy' => [
        'vocabulary' => $this->fruits->get('vid'),
      ],
    ]);

    $event = new YouTubePublishEvent($media, TRUE);
    $subscriber->doPublishMedia($event);
    $this->assertTrue($event->checkPublish());

    // Whitelisted vocabulary but no whitelisted terms, no tags - fail.
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
      'status' => TRUE,
      $videoField->getName() => [
        [
          'target_id' => $file->id(),
        ],
      ],
    ]);
    $media->getSource()->setConfiguration([
      'taxonomy' => [
        'vocabulary' => $this->fruits->get('vid'),
      ],
    ]);

    $event = new YouTubePublishEvent($media, TRUE);
    $subscriber->doPublishMedia($event);
    $this->assertFalse($event->checkPublish());

    // Whitelisted vocabulary but no whitelisted terms, wrong vocabulary - fail.
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
      'status' => TRUE,
      $videoField->getName() => [
        [
          'target_id' => $file->id(),
        ],
      ],
      'field_media_tags' => [
        Term::load(5),
      ],
    ]);
    $media->getSource()->setConfiguration([
      'taxonomy' => [
        'vocabulary' => $this->fruits->get('vid'),
      ],
    ]);

    $event = new YouTubePublishEvent($media, TRUE);
    $subscriber->doPublishMedia($event);
    $this->assertFalse($event->checkPublish());

    // Whitelisted vocabulary and terms, right vocabulary and right terms-pass.
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
      'status' => TRUE,
      $videoField->getName() => [
        [
          'target_id' => $file->id(),
        ],
      ],
      'field_media_tags' => [
        Term::load(5), Term::load(2),
      ],
    ]);
    $media->getSource()->setConfiguration([
      'taxonomy' => [
        'vocabulary' => $this->fruits->get('vid'),
        'terms' => [
          ['target_id' => '1'],
          ['target_id' => '2'],
        ],
      ],
    ]);

    $event = new YouTubePublishEvent($media, TRUE);
    $subscriber->doPublishMedia($event);
    $this->assertTrue($event->checkPublish());

    // Whitelisted vocabulary and terms, wrong terms - fail.
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
      'status' => TRUE,
      $videoField->getName() => [
        [
          'target_id' => $file->id(),
        ],
      ],
      'field_media_tags' => [
        Term::load(5), Term::load(2),
      ],
    ]);
    $media->getSource()->setConfiguration([
      'taxonomy' => [
        'vocabulary' => $this->fruits->get('vid'),
        'terms' => [
          ['target_id' => '1'],
          ['target_id' => '3'],
        ],
      ],
    ]);

    $event = new YouTubePublishEvent($media, TRUE);
    $subscriber->doPublishMedia($event);
    $this->assertFalse($event->checkPublish());

    // Vocabulary and terms, no tags - fail.
    $media = Media::create([
      'bundle' => $this->youTubeMediaType->id(),
      'status' => TRUE,
      $videoField->getName() => [
        [
          'target_id' => $file->id(),
        ],
      ],
    ]);
    $media->getSource()->setConfiguration([
      'taxonomy' => [
        'vocabulary' => $this->fruits->get('vid'),
        'terms' => [
          ['target_id' => '1'],
          ['target_id' => '3'],
        ],
      ],
    ]);

    $event = new YouTubePublishEvent($media, TRUE);
    $subscriber->doPublishMedia($event);
    $this->assertFalse($event->checkPublish());
  }

  /**
   * Creates the test vocabularies.
   */
  protected function initializeVocabularies() {
    $this->fruits = Vocabulary::create([
      'vid' => 'fruits',
      'name' => 'Fruits',
    ]);
    $this->fruits->save();
    $this->colours = Vocabulary::create([
      'vid' => 'colours',
      'name' => 'Colours',
    ]);
    $this->colours->save();
    Term::create([
      'tid' => '1',
      'name' => 'peach',
      'vid' => 'fruits',
    ])->save();
    Term::create([
      'tid' => '2',
      'name' => 'kiwi',
      'vid' => 'fruits',
    ])->save();
    Term::create([
      'tid' => '3',
      'name' => 'mango',
      'vid' => 'fruits',
    ])->save();
    Term::create([
      'tid' => '4',
      'name' => 'yellow',
      'vid' => 'colours',
    ])->save();
    Term::create([
      'tid' => '5',
      'name' => 'lavender',
      'vid' => 'colours',
    ])->save();
    Term::create([
      'tid' => '6',
      'name' => 'sky blue',
      'vid' => 'colours',
    ])->save();
  }

  /**
   * Creates a tag field.
   *
   * @return \Drupal\Core\Entity\EntityInterface|static
   *   The created tag field.
   */
  protected function createTagField() {
    $field_storage = FieldStorageConfig::create([
      'entity_type' => 'media',
      'field_name' => 'field_media_tags',
      'type' => 'entity_reference',
      'cardinality' => '-1',
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ]);
    $field_storage->save();

    // Create field for the tags.
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => $this->youTubeMediaType->id(),
      'label' => t('Tags'),
      'settings' => [
        'handler' => 'default:taxonomy_term',
        'handler_settings' => [
          'target_bundles' => [
            'fruits' => 'fruits',
            'colours' => 'colours',
          ],
        ],
      ],
    ]);
    $field->save();
    $this->youTubeMediaType->setFieldMap(['tags' => 'field_media_tags'])
      ->save();
    return $field;
  }

}
